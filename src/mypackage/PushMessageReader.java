package mypackage;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

import javax.microedition.io.Connection;

import net.rim.device.api.io.Base64InputStream;
import net.rim.device.api.io.http.HttpServerConnection;
import net.rim.device.api.io.http.PushInputStream;
import net.rim.device.api.notification.NotificationsManager;
import net.rim.device.api.system.Application;
import net.rim.device.api.system.ApplicationDescriptor;
import net.rim.device.api.system.ApplicationManager;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.CodeModuleManager;
import net.rim.device.api.ui.DrawStyle;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiEngine;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.util.Arrays;

public class PushMessageReader 
{
	// HTTP header property that carries unique push message ID
	private static final String MESSAGE_ID_HEADER = "Push-Message-ID";
	// content type constant for text messages
	private static final String MESSAGE_TYPE_TEXT = "text";
	// content type constant for image messages
	private static final String MESSAGE_TYPE_IMAGE = "image";

	private static final int MESSAGE_ID_HISTORY_LENGTH = 10;
	private static String[] messageIdHistory = new String[MESSAGE_ID_HISTORY_LENGTH];
	private static byte historyIndex;

	private static byte[] buffer = new byte[15 * 1024];
	private static byte[] imageBuffer = new byte[10 * 1024];
	
	// Long value com.samples.simpleNotification
	public static final long ID = 0x749cb23a75c60e2dL;
	
	//Pop up image
	//public static Bitmap popup = Bitmap.getBitmapResource("icon_24.png");

	/**
	 * Utility classes should have a private constructor.
	 */
	private PushMessageReader() 
	{
	}

	/**
	 * Reads the incoming push message from the given streams in the current thread and notifies controller to display the information.
	 * 
	 * @param pis
	 *            the pis
	 * @param conn
	 *            the conn
	 */
	public static void process(PushInputStream pis, Connection conn) 
	{
		System.out.println("Reading incoming push message ...");
		
		try 
		{
			HttpServerConnection httpConn;
			
			if (conn instanceof HttpServerConnection) 
			{
				httpConn = (HttpServerConnection) conn;
			} 
			else 
			{
				throw new IllegalArgumentException("Can not process non-http pushes, expected HttpServerConnection but have "
						+ conn.getClass().getName());
			}

			String msgId = httpConn.getHeaderField(MESSAGE_ID_HEADER);
			String msgType = httpConn.getType();
			String encoding = httpConn.getEncoding();

			System.out.println("Message props: ID=" + msgId + ", Type=" + msgType + ", Encoding=" + encoding);

			boolean accept = true;
			
			if (!alreadyReceived(msgId)) 
			{
				byte[] binaryData;

				if (msgId == null) 
				{
					msgId = String.valueOf(System.currentTimeMillis());
				}

				if (msgType == null) 
				{
					System.out.println("Message content type is NULL");
					accept = false;
				} 
				else if (msgType.indexOf(MESSAGE_TYPE_TEXT) >= 0) 
				{
					// A string
					int size = pis.read(buffer);
									
					binaryData = new byte[size];
					System.arraycopy(buffer, 0, binaryData, 0, size);			
					// TODO report message
							        
					//Trigger action for notification
					NotificationsManager.triggerImmediateEvent(ID, 0, null, null);
					
					//Test code here
					//String text = new String(binaryData, "UTF-8");
					//String strPass = "text : " + text;
					//processTextMessage(buffer, strPass);
					
					processTextMessage(buffer);	
				}
				else if (msgType.indexOf(MESSAGE_TYPE_IMAGE) >= 0)
				{
					// an image in binary or Base64 encoding
					int size = pis.read(buffer);
					
					if (encoding != null && encoding.equalsIgnoreCase("base64")) 
					{
						// image is in Base64 encoding, decode it
						Base64InputStream bis = new Base64InputStream(new ByteArrayInputStream(buffer, 0, size));
						size = bis.read(imageBuffer);
					}
					
					binaryData = new byte[size];
					System.arraycopy(buffer, 0, binaryData, 0, size);					
					// TODO report message
				} 
				else 
				{
					System.out.println("Unknown message type " + msgType);
					accept = false;
				}
			} 
			else 
			{
				System.out.println("Received duplicate message with ID " + msgId);
			}
			pis.accept();
		} 
		catch (Exception e) 
		{
			System.out.println("Failed to process push message: " + e);
		} 
		finally 
		{
			
			PushAgent.close(conn, pis, null);
		}
	}

	/**
	 * Check whether the message with this ID has been already received.
	 * 
	 * @param id
	 *            the id
	 * @return true, if successful
	 */
	private static boolean alreadyReceived(String id) 
	{
		if (id == null) 
		{
			return false;
		}

		if (Arrays.contains(messageIdHistory, id)) 
		{
			return true;
		}

		// new ID, append to the history (oldest element will be eliminated)
		messageIdHistory[historyIndex++] = id;
		
		if (historyIndex >= MESSAGE_ID_HISTORY_LENGTH) 
		{
			historyIndex = 0;
		}
		return false;
	}
	
	private static void processTextMessage(final byte[] data) 
	{
		String datautf8 = "";
		
		try 
		{
			datautf8 = new String(data, "UTF-8");
		} 
		catch (UnsupportedEncodingException e) 
		{
		}

		synchronized (Application.getEventLock()) 
		{
			UiEngine ui = Ui.getUiEngine();
			
			String title = datautf8.substring(0, datautf8.indexOf("@"));
			
			//Need to add another symbol # because the string being return is 
			//	far longer then expected, length = 1xxxx
			String newsid = datautf8.substring(datautf8.indexOf("@") + 1, datautf8.indexOf("#"));
			
			GlobalDialog screen = new GlobalDialog(title, newsid);
			
			ui.pushGlobalScreen(screen, 1, UiEngine.GLOBAL_QUEUE);
		}
	}

	static class GlobalDialog extends PopupScreen 
	{
		private static String yes = "是";
		private static String no = "否";
		private static BitmapField logo;
		private ButtonField yesbtn, nobtn;
		private HorizontalFieldManager hfm, hfm2;
		private String newid = "", titles = "";

		public GlobalDialog(String title, String newsid) 
		{
			super(new VerticalFieldManager());
			newid = "";
			titles = "";
			newid = newsid;
			titles = title;
			
			
			logo = new BitmapField(Bitmap.getBitmapResource("icon.png"));

			hfm = new HorizontalFieldManager(Field.FIELD_HCENTER);
			hfm.add(logo);
			hfm.add(new LabelField(titles, DrawStyle.LEFT | Field.FIELD_VCENTER));
			
			add(hfm);

			hfm2 = new HorizontalFieldManager(Field.FIELD_HCENTER);
			
			yesbtn = new ButtonField(yes, Field.FOCUSABLE | Field.FIELD_HCENTER
					| DrawStyle.VCENTER | DrawStyle.HCENTER) 
			{
				protected boolean navigationClick(int status, int time) 
				{
					try 
					{
						close();
						
						//call the application
						//Retrieve a handle for your application
						int modHandle = CodeModuleManager.getModuleHandle("OrientalDailyBB");
						
						boolean test = true;
						
						//Retrieve an array of objects that represent the 
						//		applications that are running on the device
						ApplicationDescriptor[] allApps = ApplicationManager
											.getApplicationManager().getVisibleApplications();
						
						//Examine each element of the array to 
						//		determine whether your application is running
						for(int i = allApps.length -1; i >= 0; --i) 
						{
					        if(allApps[i].getModuleHandle() == modHandle) 
					        {
					        	int procID = ApplicationManager.getApplicationManager()
					        					.getProcessId(allApps[i]);
					        	
					            ApplicationManager.getApplicationManager()
					            	.postGlobalEvent(procID, 0x947cb23a75c60e2dL, 0, 0, newid, null);
					            
					            test = false;
					        }
					    }
						
						//Launch new instance of OrientalDaily
						if(test)
						{
						//If your application is not running on the device, retrieve an 
						//		ApplicationDescriptor object that represents your application.
						ApplicationDescriptor[] myAppDes =  
									CodeModuleManager.getApplicationDescriptors(modHandle);
						
					    ApplicationManager.getApplicationManager().
					    			runApplication(new ApplicationDescriptor(myAppDes[0], 
					    					new String[]{newid}));
					       
						}	
						
						newid = "";
						titles = "";
					} 
					//catch (ApplicationManagerException e)
					catch(Exception e)
					{
					}
					return true;
				}
			};
			
			hfm2.add(yesbtn);

			nobtn = new ButtonField(no, Field.FOCUSABLE | Field.FIELD_HCENTER
					| DrawStyle.VCENTER | DrawStyle.HCENTER) 
			{
				protected boolean navigationClick(int status, int time) 
				{
					newid = "";
					titles = "";
					close();
					
					return true;
				}
			};
			hfm2.add(nobtn);
			add(hfm2);
		}

		public boolean keyDown(int keycode, int status) 
		{
			if (Keypad.key(keycode) == Keypad.KEY_ESCAPE) 
			{
				newid = "";
				titles = "";
				close();
				
				return true;
			}
				return super.keyDown(keycode, status);
		}
	}
}
